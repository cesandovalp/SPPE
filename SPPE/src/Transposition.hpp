#ifndef TRANSPOSITION_HPP
#define TRANSPOSITION_HPP

#include <vector>

template <typename Genotype>
class Transposition : public GeneticOperator<Genotype>
{
  public:
    Transposition() { this->arity = 1; }

    std::vector<Genotype> apply(std::vector<Genotype> selected)
    {
      return transpose(selected[0]);
    }

  private:
    virtual std::vector<Genotype> transpose(Genotype) = 0;
};

#endif