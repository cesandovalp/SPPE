#ifndef GENERATE_HPP
#define GENERATE_HPP

#include <vector>
#include "util.hpp"

template<typename Genotype>
Gene_pool<Genotype> generateIndividuals(int pop_size)
{
  Gene_pool<Genotype> result;
  for(int i = 0; i < pop_size; i++)
  {
    Genotype temp;
    result.push_back(temp);
  }

  return result;
}

#endif