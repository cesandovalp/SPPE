#ifndef SELECTION_HPP
#define SELECTION_HPP

#include <random>

static int tournament(double* fitness, int size)
{
  std::default_random_engine         generator(std::random_device{}());
  std::uniform_int_distribution<int> distribution(0, size - 1);

  int k, best, ind;

  k    = size / 5;
  best = -1;

  for(int i = 0; i < k; i++)
  {
    ind = distribution(generator);

    if(best == -1) best = ind;
    else
    {
      if(fitness[ind] > fitness[best])
      {
        best = ind;
      }
    }
  }

  return best;
}

#endif