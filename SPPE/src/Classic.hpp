#ifndef CLASSICGA_HPP
#define CLASSICGA_HPP

#include "GeneticAlgorithm.hpp"

template <typename Fitness, typename Genotype, typename Phenotype, typename Environment>
class Classic : public GeneticAlgorithm<Fitness, Genotype, Phenotype, Environment>
{
  public:

    Classic(GEN_FUN gen_fun, GROW_FUN grow_fun, int pop_size, OPERATORS operators)
    : GeneticAlgorithm<Fitness, Genotype, Phenotype, Environment>(gen_fun, grow_fun, pop_size, operators) {}

    Classic(GROW_FUN grow_fun, int pop_size, int generations_limit, OPERATORS operators)
    : GeneticAlgorithm<Fitness, Genotype, Phenotype, Environment>(grow_fun, pop_size, generations_limit, operators) {}

    int run(std::vector<Fitness> f, Phenotype &p, Environment env)
    {
      VEC_INT  index;
      R_DEVICE rd;
      MT_VAR   e2(rd());
      UINTDIST dist(0,100);
      double   max, min, avg;

//      cout << "--1.1" << endl;

      // Initialization
      this->pop = (*this->gen_fun)(this->pop_size);

//      cout << "--1.2" << endl;

      // Evaluation
      this->summary(max, min, avg, f[0], env);

//      cout << "--1.3" << endl;

      this->tracer.push(max, "| ");
      this->tracer.push(min, " | ");
      this->tracer.push(avg, " | ", "\n");

//      cout << "--1.4" << endl;
      
      for(int i = 0; i < this->generations_limit; i++)
      {
        bool   finish_flag;
        G_POOL child;

        finish_flag = false;

//        cout << "---1.4.1" << endl;
//        cout << "---- i = " << i << endl;

        while(!finish_flag)
        {
          int    gen_op, arity, curr;
          int*   index;
          double threshold, sum_op;
          G_POOL selected, child_t;

//          cout << "----1.4.1.1" << endl;

          //Selection of the operator
          {
            threshold = dist(e2) / 100.0;
            sum_op    = 0;
            gen_op    = -1;

//            cout << "-----1.4.1.1.1" << endl;

            while((sum_op <= threshold) && (gen_op + 1 < ((int)operators_rates.size())))
            {
              sum_op += operators_rates[++gen_op];
            }

//            cout << "-----1.4.1.1.2" << endl;
//            cout << "------ gen_op = " << gen_op << endl;
//            cout << "------ Arity = " << this->operators[gen_op]->getArity() << endl;

            index = new int[this->operators[gen_op]->getArity()];

//            cout << "-----1.4.1.1.3" << endl;
          }

//          cout << "----1.4.1.2" << endl;

          //Selection of the individuals
          for(int selection = 0; selection < this->operators[gen_op]->getArity(); selection++)
          {
            index[selection] = this->sel_fun(this->fit_eval, this->pop_size);
          }

//          cout << "----1.4.1.3" << endl;

          finish_flag = child.size() >= this->pop_size;

//          cout << "----1.4.1.4" << endl;
          
          for(int j = 0; j < this->operators[gen_op]->getArity(); j++)
          {
            selected.push_back(this->pop[index[j]]);
          }

//          cout << "----1.4.1.5" << endl;
          
          child_t = this->operators[gen_op]->apply(selected);
          
//          cout << "----1.4.1.6" << endl;
          
          arity   = this->operators[gen_op]->getArity();

//          cout << "----1.4.1.7" << endl;
          
          for(int repl = 0; repl < arity; repl++)
          {
            curr = index[repl];
            this->repl_fun(this->pop[curr], child, child_t[repl], this->fit_eval[curr], f[0], this->grow_fun, env);
          }

//          cout << "----1.4.1.8" << endl;

          delete[] index;
        }

//        cout << "--1.4.2" << endl;

        this->pop = child;

//        cout << "--1.4.3" << endl;

        this->summary(max, min, avg, f[0], env);

//        cout << "--1.4.4" << endl;
      }

//      cout << "--1.5" << endl;

      this->summary(max, min, avg, f[0], env);

//      cout << "--1.6" << endl;

      this->tracer.push(max, "| ");
      this->tracer.push(min, " | ");
      this->tracer.push(avg, " | ", "\n");

//      cout << "--1.7" << endl;

      this->selectOne(p, f[0], env);

      return 0;
    }

    void setOperatorsRates(std::vector<double> rates)
    {
      if(rates.size() != this->operators.size())
      {
        for(int i = 0; i < this->operators.size(); i++)
        {
          this->operators_rates.push_back(1.0/this->operators.size());
        }
      }
      else
      {
        this->operators_rates = rates;
      }
    }

  private:
    std::vector<double> operators_rates;
};

#endif
