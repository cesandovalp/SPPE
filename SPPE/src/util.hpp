#ifndef UTIL_HPP
#define UTIL_HPP

#define G_POOL       Gene_pool<Genotype>
#define GEN_FUN      generate_function<Genotype>
#define SEL_FUN      selection_function
#define GROW_FUN     grow_function<Phenotype, Genotype>
#define REPL_FUN     replacement_function<Genotype, Phenotype, Fitness, Environment>
#define OPERATORS    std::vector<GeneticOperator<Genotype>*>
#define OPERATORRATE std::vector< std::vector<double> >
#define VEC_INT      std::vector<int>
#define VEC_DOUBLE   std::vector<double>
#define R_DEVICE     std::random_device
#define MT_VAR       std::mt19937
#define UINTDIST     std::uniform_int_distribution<int>
#define UREALDIST    std::uniform_real_distribution<>

template <typename Genotype>
using Gene_pool = std::vector<Genotype>;

template <typename Genotype>
using generate_function = G_POOL (*) (int);

template <typename Phenotype, typename Genotype>
using grow_function = Phenotype (*) (Genotype);

template <typename Genotype, typename Phenotype, typename Fitness, typename Environment>
using replacement_function = bool (*) (Genotype, G_POOL&, Genotype, double, Fitness, GROW_FUN, Environment);

using selection_function = int (*) (double*, int);

#endif