#ifndef REPLACEMENT_HPP
#define REPLACEMENT_HPP

#include "util.hpp"

template<typename Fitness, typename Genotype, typename Phenotype, typename Environment>
bool SteadyState(Genotype p, G_POOL& c_l, Genotype child, double fit_p, Fitness f, GROW_FUN grow, Environment env)
{
  if(f(grow(child), env) > fit_p)
  {
    c_l.push_back(child);
    return true;
  }
  else
  {
    c_l.push_back(p);
    return false;
  }
}

#endif