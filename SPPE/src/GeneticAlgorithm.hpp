#ifndef GA_HPP
#define GA_HPP

#include "GeneticOperator.hpp"
#include "Generate.hpp"
#include "Replacement.hpp"
#include "util.hpp"
#include "Tracer.hpp"
#include "Optim.hpp"
#include <cmath>
#include <random>
#include <vector>
#include <string>

using std::string;

template <typename Fitness, typename Genotype, typename Phenotype, typename Environment>
class GeneticAlgorithm : public Optim<Fitness, Phenotype, Environment>
{
  public:

    GeneticAlgorithm(GEN_FUN gen_fun, GROW_FUN grow_fun, int pop_size, OPERATORS operators)
    {
      init(gen_fun, grow_fun, pop_size, 10000, operators);
    }

    GeneticAlgorithm(GROW_FUN grow_fun, int pop_size, int generations_limit, OPERATORS operators)
    {
      init(generateIndividuals, grow_fun, pop_size, generations_limit, operators);
    }

    void setSelection(SEL_FUN sel_fun)
    {
      this->sel_fun = sel_fun;
    }
    void setReplacement(REPL_FUN repl_fun)
    {
      this->repl_fun = repl_fun;
    }

    void setTracer(bool verbose)
    {
      this->tracer.setOutput(verbose);
    }

    vector<Any*> getTrace()
    {
      return this->tracer.getRawData();
    }

    void init(GEN_FUN gen_fun, GROW_FUN grow_fun, int pop_size, int generations_limit, OPERATORS operators)
    {
      this->gen_fun  = gen_fun;
      this->grow_fun = grow_fun;
      this->pop_size = pop_size;
      this->fit_eval   = new double[pop_size];

      this->generations_limit = generations_limit;

      for(int i = 0; i < operators.size(); i++)
      {
        this->operators.push_back(operators[i]);
      }
    }

    void resetTracer()
    {
      this->tracer.clear();
    }

    ~GeneticAlgorithm()
    {
      delete[] fit_eval;
    }

    protected:
      GEN_FUN   gen_fun;
      GROW_FUN  grow_fun;
      SEL_FUN   sel_fun;
      REPL_FUN  repl_fun;
      G_POOL    pop;
      double*   fit_eval;
      int       pop_size,
                generations_limit;
      OPERATORS operators;
      Tracer    tracer;

      void summary(double& max, double& min, double& avg, Fitness f, Environment env)
      {
        double sum;

        sum = 0;
        min = 99999;
        max = -99999;

//        cout << "---1.2.1" << endl;

        for(int i = 0; i < pop_size; i++)
        {
//          cout << "----1.2.1.1" << endl;

          fit_eval[i] = f( grow_fun( pop[i] ), env );

//          cout << "----1.2.1.2" << endl;

          if(fit_eval[i] > max) { max = fit_eval[i]; }

//          cout << "----1.2.1.3" << endl;

          if(fit_eval[i] < min) { min = fit_eval[i]; }

//          cout << "----1.2.1.4" << endl;

          sum += fit_eval[i];
        }

//        cout << "-----1.2.2" << endl;

        avg = sum / pop_size;
      }

      void selectOne(Phenotype& p, Fitness f, Environment env)
      {
        double min = 99999;

        for(int i = 0; i < pop_size; i++)
        {
          Phenotype temp_p = grow_fun(pop[i]);
          double    temp   = f(temp_p, env);

          if(temp < min)
          {
            min = temp;
            p   = temp_p;
          }
        }
      }
  };

#endif
