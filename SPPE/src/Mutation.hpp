#ifndef MUTATION_HPP
#define MUTATION_HPP

#include <vector>

template <typename Genotype>
class Mutation : public GeneticOperator<Genotype>
{
  public:
    Mutation() { this->arity = 1; }

    std::vector<Genotype> apply(std::vector<Genotype> selected)
    {
      return mutate(selected[0]);
    }

  private:
    virtual std::vector<Genotype> mutate(Genotype) = 0;
};

#endif