#include <Rcpp.h>
#include <iostream>
#include <vector>
#include <random>
#include "GeneticAlgorithm.hpp"
#include "Classic.hpp"
#include "Selection.hpp"
#include "Replacement.hpp"
#include "Crossover.hpp"
#include "Mutation.hpp"
#include "util.hpp"
#include "Tracer.hpp"

using namespace std;
using namespace Rcpp;

typedef vector<double>        Phenotype;
typedef vector<NumericVector> Environment_;

typedef double (*Fitness)(Phenotype, Environment_);

struct Genotype
{
  vector<double> data;

  Genotype()
  {
    random_device rd;
    mt19937 e2(rd());
    uniform_real_distribution<double> dist(0, 5);

    data.push_back( dist(e2) / 1.0 ); // p
    data.push_back( dist(e2) / 1.0 ); // q
    data.push_back( dist(e2) / 1.0 ); // d

    for(int i = 0; i < ((int) data[0]) + ((int) data[1]); i++)
    {
      data.push_back( dist(e2) / 1.0 );
    }
  }

  void copy(Genotype p)
  {
    data.clear();
    data = vector<double>( p.data.begin(), p.data.end() );
  }

  int size() { return data.size(); }

  void push_back(double x) { data.push_back(x); }

  double get(int index) { return data[index]; }

  void set(int index, double value) { data[index] = value; }

  void clear() { data.clear(); }
};

Phenotype grow(Genotype g)
{
  Phenotype temp;

  temp = g.data;

  return temp;
}

class xover_vector : public Crossover<Genotype>
{
  public:
    xover_vector(){}

    std::vector<Genotype> cross(Genotype a, Genotype b)
    {
      std::random_device rd;
      std::default_random_engine generator(rd());
      std::uniform_real_distribution<double> d2(-1.0, 1.0);

      std::vector<Genotype> result;

      Genotype data1;
      Genotype data2;

      data1.data = std::vector<double>(a.data.begin(), a.data.begin() + a.size() / 2);
      data2.data = std::vector<double>(b.data.begin(), b.data.begin() + b.size() / 2);

      std::vector<double> temp(data1.data);
      data1.data.insert(data1.data.end(), data2.data.begin(), data2.data.end());
      data2.data.insert(data2.data.end(), temp.begin(), temp.end());

      while(data1.size() < ((int)data1.data[0]) + ((int)data1.data[1]) + 3)
      {
        data1.data.push_back(d2(generator));
      }

      while(data1.size() > ((int)data1.data[0]) + ((int)data1.data[1]) + 3)
      {
        data1.data.pop_back();
      }

      while(data2.size() < ((int)data2.data[0]) + ((int)data2.data[1]) + 3)
      {
        data2.data.push_back(d2(generator));
      }

      while(data2.size() > ((int)data2.data[0]) + ((int)data2.data[1]) + 3)
      {
        data2.data.pop_back();
      }

      result.push_back(data1);
      result.push_back(data2);

      return result;
    }
};

class mutation_vector : public Mutation<Genotype>
{
  public:
    mutation_vector(){}

    std::vector<Genotype> mutate(Genotype a)
    {
      std::random_device rd;
      std::default_random_engine generator(rd());
      std::uniform_int_distribution<int> d1(0, a.size() - 1);
      std::uniform_real_distribution<double> d2(-1.0, 1.0);

      std::vector<Genotype> result;
      Genotype temp;
      temp.data = std::vector<double>(a.data.begin(), a.data.end());

      int index = d1(generator);
//      cout << "index -> " << index << " -> " << a.size() << endl;
      temp.set(index, a.get(index) + d2(generator));
      index = d1(generator);
      temp.set(index, a.get(index) + d2(generator));

      while(temp.size() < ((int)temp.data[0]) + ((int)temp.data[1]) + 3)
      {
        temp.data.push_back(d2(generator));
      }

      while(temp.size() > ((int)temp.data[0]) + ((int)temp.data[1]) + 3)
      {
        temp.data.pop_back();
      }

//      cout << "-----1.4.1.5.1" << endl;
//      cout << "----- Size = " << temp.size() << endl;
//      cout << "----- P+Q+3 = " << ((int)temp.data[0]) + ((int)temp.data[1]) + 3 << endl;

      result.push_back(temp);

//      cout << "-----1.4.1.5.2" << endl;

      return result;
    }
};

string traceToString(vector<Any*> trace)
{
  string result = "";

  for(int i = 0; i < trace.size(); i++)
  {
    if(i % 3 == 0) result += "\n";
    result += " | " + to_string(trace[i]->getData<double>());
  }

  result += "\n";

  return result;
}

double diference_operator(NumericVector y, int index, int d = 1)
{
  if(index - d <= 0)
  {
    return 0;
  }
  if(d == 0)
  {
    return y[index];
  }

  return diference_operator(y, index, d - 1)
          - diference_operator(y, index - 1, d - 1);
}

// p = phi.size()             // q = theta.size()
// y = time_series            // a = white noise
// [[Rcpp::export]]
NumericVector evaluate_parameters(NumericVector phi,
                                  NumericVector theta,
                                  int           d,
                                  NumericVector y,
                                  NumericVector a)
{
  double theta_q, phi_p;
  int    p, q;

  NumericVector result(y.size());

  theta_q = phi_p = 0;

  p = phi.size();
  q = theta.size();

  for(int t = 0; t < y.size(); t++)
  {
    for(int k = 0; k < p; ++k)
    {
      if(t-k >= 0)
      {
        phi_p += phi[k]*diference_operator(y, t - k, d);
      }
    }

    for(int k = 0; k < q; ++k)
    {
      if(t-k >= 0)
      {
        theta_q += theta[k]*a[t - k];
      }
    }

    result[t] = diference_operator(y, t, d) - a[t] - phi_p + theta_q;
  }

  return result;
}

// info[0] = y = time series
// info[1] = a = white noise
double fitness(vector<double> parameters, Environment_ info)
{
  double theta_q, phi_p;
  double sum;
  int    p, q, d;
  int    size;

  p    = (int) parameters[0];
  q    = (int) parameters[1];
  d    = (int) parameters[2];
  size = info[0].size();

//  cout << "-----1.2.1.1.1" << endl;

  vector<double> result(size);
  vector<double> phi(p);
  vector<double> theta(q);

//  cout << "-----1.2.1.1.2" << endl;
//  cout << "------ p = " << p << endl;
//  cout << "------ q = " << q << endl;
//  cout << "------ d = " << d << endl;
//  cout << "------ size(y) = " << size << endl;
//  cout << "------ size(par) = " << parameters.size() << endl;

  for(int i = 3; i < p + 3; i++)
  {
//    cout << "phi -> " << i - 3 << "/" << phi.size() << endl;
    phi[i - 3] = parameters[i];
  }

//  cout << "-----1.2.1.1.3" << endl;

//  cout << "parameters -> " << parameters.size() - (q + 2 + p) << endl;
  for(int i = 3 + p; i < q + 3 + p; i++)
  {
//    cout << "theta -> " << i - (3 + p) << "/" << theta.size() << endl;
//    cout << "parameters -> " << i - (3 + p) << "/" << theta.size() << " -> " << i << "/" << parameters.size() << endl;
    theta[i - (3 + p)] = parameters[i];
  }

//  cout << "-----1.2.1.1.4" << endl;

  sum = theta_q = phi_p = 0;

  for(int t = 0; t < size; t++)
  {
    for(int k = 0; k < p; ++k)
    {
      if(t-k >= 0)
      {
        phi_p += phi[k]*diference_operator(info[0], t - k, d);
      }
    }

    for(int k = 0; k < q; ++k)
    {
      if(t-k >= 0)
      {
        theta_q += theta[k]*info[1][t - k];
      }
    }

    result[t] = diference_operator(info[0], t, d) - info[1][t] - phi_p + theta_q;
  }

//  cout << "-----1.2.1.1.5" << endl;

  for(int i = 0; i < result.size(); i++)
  {
    sum = sqrt(result[i]*result[i]);
  }

//  cout << "-----1.2.1.1.6" << endl;
//  cout << "------ sum = " << sum << endl;

  return sum;
}

// [[Rcpp::export]]
List estimate_parameters(NumericVector y, NumericVector a, int pop_size, int iterations)
{
  //CharacterVector x_ = CharacterVector::create( "foo", "bar" );
  //NumericVector y_   = NumericVector::create( 0.0, 1.0 );
  List z             = List::create( y, a );

  String         result = "";
  vector<double> operators_rates;
  vector<Any*>   trace;

  vector<NumericVector> env;

  OPERATORS        operators; //Vector to hold the genetic operators.
  xover_vector*    xv;        //Crossover
  mutation_vector* mv;        //Mutation
  vector<Fitness>  f;         //Fitness function
  Phenotype        p;         //Where the best individual will be stored

  xv = new xover_vector();    // Crossover
  mv = new mutation_vector(); // Mutation

  operators.push_back(xv); // Crossover
  operators.push_back(mv); // Mutation

  operators_rates.push_back(0.3); // Crossover
  operators_rates.push_back(0.7); // Mutation

  env.push_back(y);
  env.push_back(a);

  f.push_back(fitness);

  Classic<Fitness, Genotype, Phenotype, Environment_> classic(grow, pop_size, iterations, operators);

  result += "Classic Steady State: Binary\n";
  classic.setSelection(tournament);
  classic.setReplacement(SteadyState<Fitness, Genotype, Phenotype, Environment_>);
  classic.setOperatorsRates(operators_rates);

//  cout << "-1------------" << endl;

  classic.run(f, p, env);

//  cout << "-2------------" << endl;

  trace = classic.getTrace();
  result += traceToString(trace);
  classic.resetTracer();

  delete xv;
  delete mv;

  return z;
}

/*
 * | t |x_t|(1-B)|(1-B)²| B | B²|
 * | 1 | 1 |  -  |  -   | 2 | 3 |
 * | 2 | 2 |  1  |  -   | 3 | 4 |
 * | 3 | 3 |  1  |  0   | 4 | 5 |
 * | 4 | 4 |  1  |  0   | 5 | 6 |
 * | 5 | 5 |  1  |  0   | 6 | 7 |
 * | 6 | 6 |  1  |  0   | 7 | - |
 * | 7 | 7 |  1  |  0   | - | - |
 */
