#ifndef TRACER_HPP
#define TRACER_HPP

#include <iostream>
#include "Generic.hpp"
#include <vector>
#include <string>

using std::vector;
using std::string;

class Tracer
{
  vector<Any*> raw_data;
  string       output_data;
  bool         verbose;

  public:
    Tracer(bool verbose = false) { this->verbose = verbose;}
    ~Tracer(){}

    template<typename T>
    void push(T var, string msg0 = "", string msg1 = "")
    {
      raw_data.push_back(new Generic<T>(var));
    	if(verbose) cout << msg0 << raw_data[raw_data.size() - 1]->getData< T >() << msg1;
    }

    void setOutput(bool verbose)
    {
      this->verbose = verbose;
    }

    void clear()
    {
      for(Any* i : raw_data)
      {
        delete i;
      }

      raw_data.clear();
    }

    vector<Any*> getRawData()
    {
      return raw_data;
    }
};

#endif