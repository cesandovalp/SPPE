// normal_distribution
#include <iostream>
#include <random>

using namespace std;

int main()
{
  const int nrolls=10000;  // number of experiments
  double number[nrolls];
  double number2[nrolls];
  double sum = 0;
  double sum2 = 0;
  double mean;
  double mean2;

  std::default_random_engine generator;
  std::normal_distribution<double> distribution(5.0,2.0);
  std::default_random_engine generator2;
  std::uniform_real_distribution<double> distribution2(0.0,10.0);

  int p[10]={};

  for (int i=0; i<nrolls; ++i)
  {
    number[i] = distribution(generator);
    number2[i] = distribution2(generator2);
    sum += number[i];
    sum2 += number2[i];
  }

  mean = sum/nrolls;
  mean2 = sum2/nrolls;

  cout << "sum = " << sum << endl;
  cout << "mean = " << mean << endl;
  cout << "sum2 = " << sum2 << endl;
  cout << "mean2 = " << mean2 << endl;

  sum = 0;
  sum2 = 0;

  for (int i=0; i<nrolls; ++i)
  {
    sum += number[i] - mean;
    sum2 += number2[i] - mean2;
  }

  mean = sum/nrolls;
  mean2 = sum2/nrolls;

  cout << "sum = " << sum << endl;
  cout << "mean = " << mean << endl;
  cout << "sum2 = " << sum2 << endl;
  cout << "mean2 = " << mean2 << endl;

  return 0;
}