#include <cmath>
#include <iostream>
#include <vector>
#include <algorithm>
#include <random>

#ifndef Pi 
#define Pi 3.141592653589793238462643 
#endif 

using std::cout;
using std::endl;
using std::vector;
using std::sort;

double LogOnePlusX(double x);
double NormalCDFInverse(double p);
double RationalApproximation(double t);
void demo();
 
double RationalApproximation(double t)
{
  // Abramowitz and Stegun formula 26.2.23.
  // The absolute value of the error should be less than 4.5 e-4.
  double c[] = {2.515517, 0.802853, 0.010328};
  double d[] = {1.432788, 0.189269, 0.001308};
  return t - ((c[2]*t + c[1])*t + c[0]) / (((d[2]*t + d[1])*t + d[0])*t + 1.0);
}
 
double normsinv(double p)
{
  if (p <= 0.0 || p >= 1.0)
  {}
 
  // See article above for explanation of this section.
  if (p < 0.5)
  {
    // F^-1(p) = - G^-1(p)
    return -RationalApproximation( sqrt(-2.0*log(p)) );
  }
  else
  {
    // F^-1(p) = G^-1(1-p)
    return RationalApproximation( sqrt(-2.0*log(1-p)) );
  }
}

double normsdist(double z)
{
  double value,sum;

  sum = z;
  value = z;

  for(int i=1; i < 100; i++)
  {
      value = (value*z*z / (2*i+1));
      sum = sum+value;
  }
  
  return 0.5 + (sum / sqrt(2*Pi)) * exp(-(z*z) / 2);
}

bool ShapiroTest(vector<double> data)
{
  int n = data.size();

  vector<double> m(n),
                 a(n);

  double  denominator,
          data_mean,
          numerator,
          epsilon,
          p_value,
          std_dev,
          alpha,
          m_sum,
          mean,
          u,
          w,
          z,
        * c;

  z = p_value = std_dev = mean = denominator = numerator = data_mean = m_sum = 0;
  u = 1/sqrt(n);
  alpha = 0.05;

  sort(data.begin(), data.end());

  for( int i = 0; i < n; i++ )
  {
    m[i] = normsinv( ((i+1) - .375) / (n + .25) );
    m_sum += m[i]*m[i];
  }

  c = new double[5];
  c[0] = 2.706056; c[1] = 4.434685; c[2] = 2.071190; c[3] = 0.147981; c[4] = 0.221157;

  a[n-1] = -(c[0]*pow(u,5))+(c[1]*pow(u,4))-(c[2]*pow(u,3))-(c[3]*pow(u,2))+(c[4]*u)+(m[n-1]*pow(m_sum,-0.5));

  c[0] = 3.582633; c[1] = 5.682633; c[2] = 1.752461; c[3] = 0.293762; c[4] = 0.042981;

  a[n-2] = -(c[0]*pow(u,5))+(c[1]*pow(u,4))-(c[2]*pow(u,3))-(c[3]*pow(u,2))+(c[4]*u)+(m[n-2]*pow(m_sum,-0.5));

  a[0] = -a[n-1];
  a[1] = -a[n-2];

  epsilon = (m_sum - (2*m[n-1]*m[n-1]) - (2*m[n-2]*m[n-2]))/(1 - (2*a[n-1]*a[n-1]) - (2*a[n-2]*a[n-2]));

  for( int i = 1; i < n-2; i++ ) { a[i] = m[i]/sqrt(epsilon); }
  
  for( double i : data ) { data_mean += i; }
  
  data_mean /= n;
  
  for( int i = 0; i < n; i++ )
  {
    numerator   += a[i]*data[i];
    denominator += pow(data[i] - data_mean, 2);
  }

  w       = (numerator*numerator)/denominator;
  mean    = (0.0038915*pow((log(n)),3)) - (0.083751*pow((log(n)),2)) - (0.31082*log(n)) - 1.5861;
  std_dev = exp((0.0030302*pow(log(n), 2)) - (0.082676*log(n)) - 0.4803);
  z       = (log(1-w) - mean)/std_dev;
  p_value = 1 - normsdist(z);

  cout << "p_value = " << p_value << endl;

  return (p_value > alpha);
}

void test1()
{
  vector<double> data =  { 65, 61, 63, 86, 70, 55, 74, 35, 72, 68, 45, 58 };
  
  cout << "Shapiro Test = " << (ShapiroTest(data) ? "No" : "Yes") << endl ;
}

void test2()
{
  std::default_random_engine generator;
  std::normal_distribution<double> distribution(5.0,2.0);

  vector<double> data;

  for (int i=0; i<500; ++i)
  {
    data.push_back(distribution(generator));
  }
  
  cout << "Shapiro Test = " << (ShapiroTest(data) ? "No" : "Yes") << endl ;
}

void test3()
{
  std::default_random_engine generator;
  std::uniform_real_distribution<double> distribution(0.0,10.0);

  vector<double> data;

  for (int i=0; i<500; ++i)
  {
    data.push_back(distribution(generator));
  }
  
  cout << "Shapiro Test = " << (ShapiroTest(data) ? "No" : "Yes") << endl ;
}

void test4()
{
  vector<double> data;

  for (int i=0; i<250; ++i) { data.push_back(-1); }
  for (int i=0; i<250; ++i) { data.push_back(1); }
  
  cout << "Shapiro Test = " << (ShapiroTest(data) ? "No" : "Yes") << endl ;
}

int main()
{
  test1();
  test2();
  test3();
  test4();
  
  return 0;
}